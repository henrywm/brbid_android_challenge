package com.example.henry.brbid_android_challenge.ui.vehicles

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.henry.brbid_android_challenge.interfaces.ListItemHandler
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import kotlinx.android.synthetic.main.vehicle_item_list.view.*

class VehicleViewHolder(val view: View, private val listItemHandler: ListItemHandler.HandleItemClick) : RecyclerView.ViewHolder(view), View.OnClickListener{

    init {
        view.vehicleItemCardview.setOnClickListener(this)
    }

    fun bind(vehicle: Vehicle){
        view.vehicleItemBrand.text = vehicle.brand
        view.vehicleItemModel.text = vehicle.model
    }

    override fun onClick(v: View?) = listItemHandler.onCLick(v, adapterPosition)
}