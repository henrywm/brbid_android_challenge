package com.example.henry.brbid_android_challenge.interfaces

import android.support.v7.widget.RecyclerView
import android.view.View

interface ListItemHandler {

    interface HandleItemClick {
        fun onCLick(view: View?, position: Int)
    }

    interface HandleItemSwipe {
        fun onSwipe(holder: RecyclerView.ViewHolder, direction: Int, position: Int)
    }
}