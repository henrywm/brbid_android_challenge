package com.example.henry.brbid_android_challenge.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "vehicles")
@Parcelize
class Vehicle(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        var model: String = "",
        var color: String = "",
        var brand: String = "",
        var year: Int = 0,
        var license_plate: String = ""
) : Parcelable