package com.example.henry.brbid_android_challenge.ui.vehicles

import android.app.ActivityOptions
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.example.henry.brbid_android_challenge.R
import com.example.henry.brbid_android_challenge.helper.SwipeItemHandlerHelper
import com.example.henry.brbid_android_challenge.interfaces.ListItemHandler
import com.example.henry.brbid_android_challenge.ui.newvehicle.NewVehicleActivity
import com.example.henry.brbid_android_challenge.ui.vehicledetails.VehicleDetailsActivity
import android.util.Pair
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ListItemHandler.HandleItemClick, ListItemHandler.HandleItemSwipe{

    private val vehicleList : RecyclerView by lazy{ findViewById<RecyclerView>(R.id.vehicleRecycleView) }
    private val vehicleListAdapter : VehicleListAdapter by lazy { VehicleListAdapter(this) }
    private val vehicleViewModel : VehicleViewModel by lazy {
        ViewModelProviders.of(this).get(VehicleViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "Vehicles"
        setSupportActionBar(toolbar)
        setupList()
        vehicleViewModel.all()?.observe(this, Observer {
            val vehicles = it?.toMutableList()
            if(vehicles?.size != 0){
                messageNoVehicles.visibility = View.GONE
                vehicleListAdapter.update(vehicles!!)
            }else{
                messageNoVehicles.visibility = View.VISIBLE
            }
        })
    }

    private fun setupList(){
        vehicleList.layoutManager = LinearLayoutManager(this)
        vehicleList.itemAnimator = DefaultItemAnimator()
        vehicleList.adapter = vehicleListAdapter
        val swipeItemHandlerHelper = SwipeItemHandlerHelper(0, ItemTouchHelper.LEFT, this)
        val itemTouchHelper = ItemTouchHelper(swipeItemHandlerHelper)
        itemTouchHelper.attachToRecyclerView(vehicleList)
    }

    override fun onCLick(view: View?, position: Int) {
        val vehicle = vehicleListAdapter.dataset[position]
        val sharedIntent = Intent(this, VehicleDetailsActivity::class.java)
        val vehicleBrand = Pair<View, String>(vehicleList.getChildAt(position), "brandTransition")
        val options = ActivityOptions.makeSceneTransitionAnimation(this, vehicleBrand)
        sharedIntent.putExtra("EXTRA_VEHICLE", vehicle)
        startActivity(sharedIntent, options.toBundle())
    }

    fun newVehicle(v: View) = startActivity(Intent(this, NewVehicleActivity::class.java))

    override fun onSwipe(holder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if(holder is VehicleViewHolder){
            val brand = vehicleListAdapter.dataset[position].brand
            val deletedIndex = holder.adapterPosition
            val deletedItem = vehicleListAdapter.dataset[holder.adapterPosition]
            vehicleViewModel.delete(deletedItem)
            vehicleListAdapter.remove(deletedIndex)

            Snackbar.make(coordinatorLayout, "$brand was deleted!", Snackbar.LENGTH_LONG)
                    .setAction("Undo") {
                        vehicleListAdapter.restore(deletedItem, deletedIndex)
                        vehicleViewModel.save(deletedItem)
                    }.setActionTextColor(Color.YELLOW).show()
        }
    }

}
