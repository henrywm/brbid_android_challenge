package com.example.henry.brbid_android_challenge.model.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.henry.brbid_android_challenge.model.dao.VehicleDao
import com.example.henry.brbid_android_challenge.model.entity.Vehicle

@Database(entities = [Vehicle::class], version = VehicleDatabase.DATABASE_VERSION)
abstract class VehicleDatabase : RoomDatabase(){

    abstract fun vehicleDao(): VehicleDao

    companion object {
        const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "vehicle.db"

        var instance: VehicleDatabase? = null

        fun build(context: Context): VehicleDatabase?{
            if(instance == null){
                synchronized(VehicleDatabase::class.java){
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            VehicleDatabase::class.java,
                            DATABASE_NAME
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return instance
        }

        fun destroy(){
            instance = null
        }
    }

}