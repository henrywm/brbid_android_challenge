package com.example.henry.brbid_android_challenge.model.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.example.henry.brbid_android_challenge.model.entity.Vehicle

@Dao
interface VehicleDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vehicle: Vehicle)

    @Query("SELECT * FROM vehicles WHERE id = :id")
    fun getVehicle(id: Int): Vehicle

    @Query("SELECT * FROM vehicles ORDER BY id DESC")
    fun allVehicles(): LiveData<List<Vehicle>>

    @Delete
    fun deleteVehicle(vehicle: Vehicle)

    @Update
    fun updateVehicle(vehicle: Vehicle)
}