package com.example.henry.brbid_android_challenge.ui.vehicledetails

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.henry.brbid_android_challenge.R
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import com.example.henry.brbid_android_challenge.ui.newvehicle.NewVehicleActivity
import kotlinx.android.synthetic.main.activity_vehicle_details.*

class VehicleDetailsActivity : AppCompatActivity() {
    var vehicle: Vehicle? = null

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_details)
        val toolbar = findViewById<Toolbar>(R.id.toolbarDetails)
        toolbar.title = getString(R.string.details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        getExtras()
    }

    private fun getExtras(){
        with(intent.extras){
            vehicle = this.get("EXTRA_VEHICLE") as Vehicle
            vehicleDetailBrand.text = vehicle?.brand
            vehicleDetailModel.text = vehicle?.model
            vehicleDetailColor.text = vehicle?.color
            vehicleDetailYear.text = vehicle?.year.toString()
            vehicleDetailLicensePlate.text = vehicle?.license_plate
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.vehicle_details_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.edit_vehicle -> {
                val intent = Intent(this, NewVehicleActivity::class.java)
                intent.putExtra("EXTRA_VEHICLE", vehicle)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
