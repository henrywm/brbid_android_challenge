package com.example.henry.brbid_android_challenge.ui.vehicles

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.henry.brbid_android_challenge.R
import com.example.henry.brbid_android_challenge.interfaces.ListItemHandler
import com.example.henry.brbid_android_challenge.model.entity.Vehicle

class VehicleListAdapter(private val itemHandler: ListItemHandler.HandleItemClick) : RecyclerView.Adapter<VehicleViewHolder>(){

    val dataset: MutableList<Vehicle> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vehicle_item_list, parent, false)
        return VehicleViewHolder(view, itemHandler)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) = holder.bind(dataset[position])

    override fun getItemCount() = dataset.count()

    fun update(data: MutableList<Vehicle>){
        dataset.clear()
        dataset.addAll(data)
        notifyDataSetChanged()
    }

    fun remove(position: Int){
        dataset.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restore(vehicle: Vehicle, position: Int){
        dataset.add(position, vehicle)
        notifyItemInserted(position)
    }

}
