package com.example.henry.brbid_android_challenge.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.AsyncTask
import com.example.henry.brbid_android_challenge.model.dao.VehicleDao
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import com.example.henry.brbid_android_challenge.model.local.VehicleDatabase

class VehicleRepository(context: Context){

    private val vehicleDao: VehicleDao?
    private var vehicles: LiveData<List<Vehicle>>? = MutableLiveData<List<Vehicle>>()
    private var db: VehicleDatabase? = null

    init {
        db = VehicleDatabase.build(context)
        vehicleDao = db?.vehicleDao()
        vehicles = vehicleDao?.allVehicles()
    }

    fun save(vehicle: Vehicle) = SaveAsync(vehicleDao).execute(vehicle)

    fun all(): LiveData<List<Vehicle>>? = vehicles

    fun delete(vehicle: Vehicle) = DeleteAsync(vehicleDao).execute(vehicle)

    fun update(vehicle: Vehicle?) = UpdateAsync(vehicleDao).execute(vehicle)

    // ASYNC TASKS
    private class SaveAsync(private val vehicleDao: VehicleDao?): AsyncTask<Vehicle?, Unit, Unit>(){
        override fun doInBackground(vararg params: Vehicle?) {
            val vehicle = params[0] as Vehicle
            vehicleDao?.save(vehicle)
        }
    }
    private class DeleteAsync(private val vehicleDao: VehicleDao?): AsyncTask<Vehicle?, Unit, Unit>(){
        override fun doInBackground(vararg params: Vehicle?) {
            val vehicle = params[0] as Vehicle
            vehicleDao?.deleteVehicle(vehicle)
        }
    }

    private class UpdateAsync(private val vehicleDao: VehicleDao?): AsyncTask<Vehicle?, Unit, Unit>(){
        override fun doInBackground(vararg params: Vehicle?) {
            val vehicle = params[0] as Vehicle
            vehicleDao?.updateVehicle(vehicle)
        }
    }


}