package com.example.henry.brbid_android_challenge.ui.vehicles

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import com.example.henry.brbid_android_challenge.repository.VehicleRepository

class VehicleViewModel(app: Application) : AndroidViewModel(app){

    private val vehicleRepository: VehicleRepository = VehicleRepository(app.applicationContext)
    private var vehicles: LiveData<List<Vehicle>>? = MutableLiveData()

    init {
        vehicles = vehicleRepository.all()
    }

    fun save(vehicle: Vehicle) = vehicleRepository.save(vehicle)
    fun all() = vehicles
    fun delete(vehicle: Vehicle) = vehicleRepository.delete(vehicle)
    fun update(vehicle: Vehicle?) = vehicleRepository.update(vehicle)

    fun setVehicles(data: List<Vehicle>){
        val newList = MutableLiveData<List<Vehicle>>()
        newList.postValue(data)
        vehicles = newList
    }

}