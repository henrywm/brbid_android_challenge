package com.example.henry.brbid_android_challenge.ui.newvehicle

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.henry.brbid_android_challenge.R
import com.example.henry.brbid_android_challenge.model.entity.Vehicle
import com.example.henry.brbid_android_challenge.ui.vehicles.MainActivity
import com.example.henry.brbid_android_challenge.ui.vehicles.VehicleViewModel
import kotlinx.android.synthetic.main.activity_new_vehicle.*

class NewVehicleActivity : AppCompatActivity(){

    private val vehicleViewModel: VehicleViewModel by lazy {
        ViewModelProviders.of(this).get(VehicleViewModel::class.java)
    }
    private var vehicle: Vehicle? = null

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_vehicle)
        val toolbar = findViewById<Toolbar>(R.id.toolbarNewVehicle)
        toolbar.title = getString(R.string.new_vehicle)
        setSupportActionBar(toolbar)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        getExtras()
    }

    fun saveVehicle(view: View){
        val model = newVehicleModel.text.toString()
        val color = newVehicleColor.text.toString()
        val year = newVehicleYear.text.toString()
        val brand = newVehicleBrand.text.toString()
        val licensePlate = newVehicleLicensePlate.text.toString()
        if(checkFields(model, color, year, brand, licensePlate)){
            val v = Vehicle(
                    model = model,
                    color = color,
                    year = year.trim().toInt(),
                    brand = brand,
                    license_plate = licensePlate)
            if(vehicle == null){
                vehicleViewModel.save(v)
            }else{
                vehicleViewModel.update(v)
            }
            startActivity(Intent(this, MainActivity::class.java))
        }else{
            AlertDialog.Builder(this)
                    .setMessage(getString(R.string.all_fields_are_required))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->  }
                    .show()
        }
    }

    private fun checkFields(model: String, color: String, year: String, brand: String, licensePlate: String): Boolean{
        return !model.trim().isEmpty()
                && !color.trim().isEmpty()
                && !year.trim().isEmpty()
                && !brand.trim().isEmpty()
                && !licensePlate.trim().isEmpty()
    }

    private fun getExtras(){
        with(intent.extras){
            if(this != null){
                vehicle = this.get("EXTRA_VEHICLE") as Vehicle
                newVehicleBrand.text.insert(0, vehicle?.brand)
                newVehicleModel.text.insert(0, vehicle?.model)
                newVehicleColor.text.insert(0, vehicle?.color)
                newVehicleYear.text.insert(0, vehicle?.year.toString())
                newVehicleLicensePlate.text.insert(0, vehicle?.license_plate)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
